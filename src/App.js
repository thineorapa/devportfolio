import React from "react";
import "./App.css";
import "./animate.css";
import "react-bulma-components/dist/react-bulma-components.min.css";
import Nav from "./components/Nav";
import About from "./components/About";
import Projects from "./components/Projects";
import Contact from "./components/Contact";

function App() {
  return (
    <div className="App" id="home">
      <header className="App-header">
        <p className="hello animated slideInLeft">
          Hello, I'm <strong className="thine">Kristhine Orapa</strong>
        </p>
        <div className="full has-text-warning animated slideInRight" id="stack">
          A full-stack Web Developer
        </div>
        <div>
          <a
            href="#about"
            className="button is-warning is-outlined animated flash"
          >
            Know more about me and my works
          </a>
        </div>
      </header>

      <Nav />

      <About />
      <Projects />
      <Contact />
    </div>
  );
}

export default App;
