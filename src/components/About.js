import React from "react";
import {
	Container,
	Heading,
	Columns,
	Section,
	Image,
	Progress
} from "react-bulma-components";

const About = () => {
	return (
		<Container className="has-text-centered" id="about">
			<Heading size={2}>
				<p className="heading-text animated bounce passion">
					Web Development is now my passion
				</p>
			</Heading>
			<Section>
				<div>
					<img
						src="/images/me.jpg"
						className="animated slideInLeft"
						id="img"
						alt="profilepic"
					/>
				</div>
			</Section>
			<Section>
				<div id="par">
					<p className="has-text-left animated slideInRight has-text-justify">
						Hi, I am Kristhine Orapa you can call me thine. Allow me
						to help your company, as a Full Stack Web Developer. I
						am an optimistic and value-driven person. I came across
						exploring Wordpress when I was a college student wherein
						during my spare time I spent beautifying my online web
						store and began to fall in love in designing until my
						curiosity led me to Coding Bootcamp that I decided to
						quit my Bachelor's Degree and enroll myself to an
						intensive coding bootcamp. I took that leap and indulge
						myself learning the skills. I never regret it and now I
						want to pursue this as a career
					</p>
				</div>
			</Section>
			<Section>
				<Container fluid>
					<Heading className="notification is-dark animated flash tech">
						<p size={3} className="tech1">
							Technologies
						</p>
					</Heading>
				</Container>
			</Section>
			<Columns breakpoint="mobile" className="iconstech">
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/react.png"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/node.png"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/laravel.png"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/php.png "
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/graph.png"
						/>
					</div>
				</Columns.Column>
			</Columns>

			<Columns breakpoint="mobile">
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src=" /images/javas.png"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/html5.png"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/css.jpg"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/mysql.jpg"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/mongodb.png"
						/>
					</div>
				</Columns.Column>
			</Columns>

			<Columns breakpoint="mobile">
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/bootstrap.png"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/sass.jpg"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/Git.png"
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/heroku.png "
						/>
					</div>
				</Columns.Column>
				<Columns.Column>
					<div>
						<Image
							// className="image is-128x128"
							src="/images/github.jpg"
						/>
					</div>
				</Columns.Column>
			</Columns>

			<Columns>
				<Columns.Column className="skills">
					<Heading size={2} renderAs="p" className="skill">
						Skills
					</Heading>
					<div>
						<p className="has-text-left">React.JS</p>
						<Progress
							max={100}
							value={75}
							color="info"
							size="medium"
						/>

						<p className="has-text-left">Javascript</p>
						<Progress
							max={100}
							value={65}
							color="danger"
							size="medium"
						/>

						<p className="has-text-left">Laravel</p>
						<Progress
							max={100}
							value={70}
							color="success"
							size="medium"
						/>

						<p className="has-text-left">PHP</p>
						<Progress
							max={100}
							value={65}
							color="info"
							size="medium"
						/>

						<p className="has-text-left">CSS3</p>
						<Progress
							max={100}
							value={80}
							color="primary"
							size="medium"
						/>

						<p className="has-text-left">Bootstrap4</p>
						<Progress
							max={100}
							value={70}
							color="error"
							size="medium"
						/>
					</div>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default About;
