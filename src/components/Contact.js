import React, {useState} from "react";
import emailjs from 'emailjs-com';
import Swal from 'sweetalert2';
import {
	Columns,
	Hero,
	Footer,
	Heading,
	Section
} from "react-bulma-components";

const Contact = () => {

	const [email, setEmail] = useState('')
	const [name, setName] = useState('')
	const [message, setMessage] = useState('')

	const handleChange = (e) => {
		let { id, value } = e.target

		if (id === "name") {
			setName(value)
		}

		if (id === "email") {
			setEmail(value)
		}

		if (id === "message") {
			setMessage(value)
		}
	}

	const handleSubmit = (e) => {
		e.preventDefault()

		let  template_params = {
			"to_email": "kristhineorapa@gmail.com",
			"from_name": name,
			"reply_to": email,
			"to_name": "Thine",
			"message_html": message
		 }

		emailjs.send('thineorapa', 'thine_template', template_params, 'user_xom5wJLn7DAQyFiL1HpqV')
			.then((result) => {
				Swal.fire('I got your message. I\'ll get back to you as soon as possible. Thanks!')
					.then(() => {
						setName('');
						setEmail('');
						setMessage('');
						window.location.href = "https://thineorapa-devportfolio.herokuapp.com";
					})

			}, (error) => {
				console.log(error.text);
				Swal.fire({
					icon: 'warning',
					text: 'Oooopss.. something wen\'t wrong. Try again later.'
				})
			});
	}



	return (
		<Section id="contact">
			<Heading className="proj">Let's Get Connected</Heading>
			<Columns className="cont">
				{/*left side*/}
				<Columns.Column className="animated fadeInUp">
					<form onSubmit={handleSubmit} encType={'multipart/form-data'}>
						<div className="field">
							<label for="name">Name</label>
							<div className="control">
								<input
									id="name"
									className="input"
									type="text"
									onChange={handleChange}
									value={name} 
								/>
							</div>
						</div>
						<div className="field">
							<label for="email">Email</label>
							<div className="control">
								<input
									id="email"
									className="input"
									type="text"
									onChange={handleChange}
									value={email} 
								/>
							</div>
						</div>
						<div className="field">
							<label for="message">Message</label>
							<div className="control">
								<textarea
									id="message"
									className="textarea"
									placeholder="Input message here"
									onChange={handleChange}
									value={message} 
								></textarea>
							</div>
						</div>

						<button className="button is-info is-fullwidth">
							Send
						</button>
					</form>
				</Columns.Column>

				{/*right side*/}
				<Columns.Column className="animated fadeInDown">
					<iframe
						id="gmap"
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d30865.74922111373!2d121.00035669934717!3d14.75670779115886!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397b1bf1440381d%3A0xab653b08fe7ecd6b!2sBarangay%20171%2C%20Caloocan%2C%20Metro%20Manila!5e0!3m2!1sen!2sph!4v1574225097670!5m2!1sen!2sph"
						frameborder="0"
						style={{ border: 0 }}
						allowfullscreen
					></iframe>
				</Columns.Column>
			</Columns>		
			<hr />
			<Hero className="animated flash">
				<Hero.Footer>
				<a
						href="https://www.linkedin.com/in/kristhine-orapa-212981187/"
						target="_blank"
					>
						<i className="fab fa-linkedin con"></i>
					</a>
					<a
						href="https://www.facebook.com/thine.orapa"
						target="_blank"
					>
						<i className="fab fa-facebook con"></i>
					</a>
					<a
						href="https://www.instagram.com/itsmethineorapa/"
						target="_blank"
					>
						<i className="fab fa-instagram con"></i>
					</a>

					<a
						href="https://accounts.google.com/SignOutOptions?hl=en&continue=https://mail.google.com/mail&service=mail"
						target="_blank"
					>
						<i className="far fa-envelope con"></i>
					</a>
					<div className="number">+63 9458068573</div>
					<p className="foot">
						<strong>Kristhine Orapa © 2019</strong>
					</p>
				</Hero.Footer>
			</Hero>
		</Section>
	);
};

export default Contact;
