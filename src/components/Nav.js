import React, { useState } from "react";
import { Navbar } from "react-bulma-components";

const NavBar = props => {
	const [open, setOpen] = useState(false);

	return (
		<Navbar color="dark" active={open} className="is-fixed-top" id="navBar">
			<Navbar.Brand>
				<a
					href="https://docs.google.com/document/d/1z5Gaxx1JyCxmIj2svawKq_0PPhcuz9yX-NDTAbjko4k/edit?usp=sharing"
					target="_blank"
					className="navbar-item resume"
				>
					<i className="fas fa-file-download"></i>
					<strong className="fas">Resume</strong>
				</a>

				<Navbar.Burger
					active={open}
					onClick={() => {
						setOpen(!open);
					}}
				/>
			</Navbar.Brand>

			<Navbar.Menu>
				<Navbar.Container position="end">
					<a href="#home" className="navbar-item">
						Home
					</a>
					<a href="#about" className="navbar-item">
						About
					</a>
					<a href="#projects" className="navbar-item">
						Projects
					</a>
					<a href="#contact" className="navbar-item">
						Contact
					</a>
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
	);
};

export default NavBar;
