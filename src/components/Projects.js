import React from "react";
import {
	Container,
	Image,
	Tile,
	Heading,
	Tag,
	Columns
} from "react-bulma-components";

const Projects = () => {
	return (
		<div className="App-header project">
			<Container className="has-text-centered" id="projects">
				<Heading size={2}>
					<p className="has-text-light animated flash proj1">
						My Projects
					</p>
				</Heading>
				<Columns>
					<Columns.Column>
						<Tile
							kind="parent"
							className="animated fadeInLeft tile"
						>
							<Tile
								renderAs="article"
								kind="child"
								notification
								color="dark"
							>
								<Heading className="has-text-warning over">
									Overcome Anxiety (Static Wesbite)
								</Heading>
								<Heading className="has-text-light" subtitle>
									This project is only a static page and a
									mobile responsive website using HTML5 and
									CSS3.
								</Heading>
								<Image src="/images/cp1.png" />
								<Tag color="black" className="tagcon">
									<p>HTML5</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>CSS3</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>Git</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>Bootstrap</p>
								</Tag>
								<div>
									<Tag color="warning" className="tagcon">
										<a
											href="https://thineorapa.gitlab.io/my-first-live-website/"
											target="_blank"
											className="cp"
										>
											View Project
										</a>
									</Tag>
								</div>
							</Tile>
						</Tile>
					</Columns.Column>
					<Columns.Column>
						<Tile kind="parent" className="animated fadeInUp">
							<Tile
								renderAs="article"
								kind="child"
								notification
								color="dark"
							>
								<Heading className="has-text-warning over">
									Super Mario Bros Race
								</Heading>
								<Heading className="has-text-light" subtitle>
									Made using jQuery. This is a mario brothers race game wherein a player has an option
									to choose the level of game to easy, medium and hard. 
								</Heading>
								<Image src="/images/mariobros.png" id="mrio" />
								<Tag color="black" className="tagcon">
									<p>JavaScript</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>jQuery</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>CSS3</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>HTML5</p>
								</Tag>
								<div>
									<Tag color="warning" className="tagcon">
										<a
											href="https://thineorapa.github.io/mario-bros-race/"
											target="_blank"
											className="cp"
										>
											View Project
										</a>
									</Tag>
								</div>
							</Tile>
						</Tile>
					</Columns.Column>
				</Columns>
				<Columns>
					<Columns.Column>
						<Tile kind="parent" className="animated fadeInUp">
							<Tile
								renderAs="article"
								kind="child"
								notification
								color="dark"
							>
								<Heading className="has-text-warning over">
									Asset Management
								</Heading>
								<Heading className="has-text-light" subtitle>
									This project is a CRUD Asset Management
									using Laravel Framework and MySQL Database.
								</Heading>
								<Image src="/images/cp2.png" />
								<Tag color="black" className="tagcon">
									<p>Laravel</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>PHP</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>MySQL</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>JavaScript</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>CSS3</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>Heroku</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>Bootstrap</p>
								</Tag>
								<div>
									<Tag color="warning" className="tagcon">
										<a
											href="http://joyride-cp2.herokuapp.com"
											target="_blank"
											className="cp"
										>
											View Project
										</a>
									</Tag>
								</div>
							</Tile>
						</Tile>
					</Columns.Column>
					<Columns.Column>
						<Tile kind="parent" className="animated fadeInRight">
							<Tile
								renderAs="article"
								kind="child"
								notification
								color="dark"
							>
								<Heading className="has-text-warning over">
									Spa Booking System
								</Heading>
								<Heading className="has-text-light" subtitle>
									This project is a Booking System Wesbite
									using ReactJS, GraphQL and NoSQL database.
								</Heading>
								<Image src="/images/booking.png" />
								<Tag color="black" className="tagcon">
									<p>ReactJS</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>JavaScript</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>MongoDB</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>GraphQL</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>Bootstrap</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>CSS3</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>Heroku</p>
								</Tag>
								<Tag color="black" className="tagcon">
									<p>Git</p>
								</Tag>
								<div>
									<Tag color="warning" className="tagcon">
										<a
											href="https://dream-spa.herokuapp.com/"
											target="_blank"
											className="cp"
										>
											View Project
										</a>
									</Tag>
								</div>
							</Tile>
						</Tile>
					</Columns.Column>
				</Columns>
			</Container>
		</div>
	);
};
export default Projects;
